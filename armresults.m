%naive cycles = l12, d1 acc = 13, d1 hits =14, s1 acc = 23, s1 hits =24
% prefill cycles = l12, d1 acc=14, d1 hits=15, s1 acc = 24, s1 hits = 25
% tagp cycles = 12, d1 acc = 14, d1 hits = 15, s1 acc = 24, s1 hits = 25
% window cycles = 12 d1 acc = 14, d1 hits = 15, s1 acc = 24, s1 hits = 25
% org cycles = 10, d1 acc= 11, d1 hits = 12
%% mibench
benchmarks =  {'adpcm_decode', 'adpcm_encode','basicmath_small','bitcnts','blowfish_decode','blowfish_encode','CRC32','dijkstra','fft','fft_inv','ghostscript','gsm_decode','ispell','jpeg_decode','jpeg_encode','lame','patricia','qsort_small','rijndael_decode','rijndael_encode','rsynth','sha','stringsearch','susan_corners','susan_edges','susan_smoothing','tiff2bw','tiff2rgba','tiffdither','tiffmedian'}

%[ocs64_32_2,ohs64_32_2,oshs64_32_2,stackpercentage] = readall('simple/armresults/orgstack/dl1_64_32_2/',2);
[ocs32_32_4,ohs32_32_4,oshs32_32_4] = readarm('simple/armresults/original/dl1_32_32_4/',2);
%[ocs128_32_1,ohs128_32_1,oshs128_32_1] = readmibench('simple/armresults/orgstack/dl1_128_32_1/',2);
[ocs64_32_4,ohs64_32_4,oshs64_32_4] = readarm('simple/armresults/original/dl1_64_32_4/',2);

hitrate = [ohs32_32_4',ohs32_32_4',ohs32_32_4']*100;
orgcycles = [ocs32_32_4',ocs32_32_4',ocs32_32_4'];
kb8cycles = [ocs64_32_4',ocs64_32_4',ocs64_32_4'];
kb8speedup = (orgcycles./kb8cycles-1)*100;
kb8speedup(23,1)=0;
kb8speedup(23,2)=0;
kb8speedup(23,3)=0;
figure(1)
clf
bar(kb8speedup);
set(gca,'XTick',1:30,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:30,'XTickLabel',benchmarks)

%% naive mibench
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1, stackpercentage] = readarm('simple/armresults/naive/dl1_32_32_4_sl1_32_32_1/',1);
%[nc32_32_4_16_32_1,nh32_32_2_16_32_1,nhs32_32_4_16_32_1] = readarm('simple/armresults/naive/dl1_32_32_4_sl1_16_32_1/',1);
%[nc32_32_4_8_32_1,nh32_32_4_8_32_1,nhs32_32_4_8_32_1] = readarm('simple/armresults/naive/dl1_32_32_4_sl1_8_32_1/',1);

%stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs32_32_4_8_32_1']*100;
%hitrate = [nh32_32_4_32_32_1',nh32_32_2_16_32_1',nh32_32_4_8_32_1']*100;
%naivecycles = [nc32_32_4_32_32_1',nc32_32_4_16_32_1',nc32_32_4_8_32_1'];
stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_32_32_1',nhs32_32_4_32_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_4_32_32_1',nh32_32_4_32_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc32_32_4_32_32_1',nc32_32_4_32_32_1'];
speedupnaive = (orgcycles./naivecycles-1)*100;
%figure(1)
%clf
%bar(stackpercentage'*100)
%title('stackrate');
%ylim([0,100]);
%set(gca,'XTick',1:30,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:30,'XTickLabel',benchmarks)

%%
figure(1)
bar(speedupnaive);
title('speedup');
set(gca,'XTick',1:30,'XTickLabel',benchmarks)

%% prefill mibench
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1] = readarm('simple/armresults/prefill/dl1_32_32_4_sl1_32_32_1/',3);
[nc32_32_4_16_32_1,nh32_32_2_16_32_1,nhs32_32_4_16_32_1] = readarm('simple/armresults/prefill/dl1_32_32_4_sl1_16_32_1/',3);
[nc32_32_4_8_32_1,nh32_32_4_8_32_1,nhs32_32_4_8_32_1] = readarm('simple/armresults/prefill/dl1_32_32_4_sl1_8_32_1/',3);

stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs32_32_4_8_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_2_16_32_1',nh32_32_4_8_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc32_32_4_16_32_1',nc32_32_4_8_32_1'];
speedupprefill = (orgcycles./naivecycles-1)*100;
figure(1)
clf
bar(speedupprefill)
title('sspeedup');
set(gca,'XTick',1:30,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:30,'XTickLabel',benchmarks)

%% window mibench
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1] = readarm('simple/armresults/window/dl1_32_32_4_sl1_32_32_1/',3);
[nc32_32_4_16_32_1,nh32_32_2_16_32_1,nhs32_32_4_16_32_1] = readarm('simple/armresults/window/dl1_32_32_4_sl1_16_32_1/',3);
[nc32_32_4_8_32_1,nh32_32_4_8_32_1,nhs32_32_4_8_32_1] = readarm('simple/armresults/window/dl1_32_32_4_sl1_8_32_1/',3);

stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs32_32_4_8_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_2_16_32_1',nh32_32_4_8_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc32_32_4_16_32_1',nc32_32_4_8_32_1'];
speedupwindow = (orgcycles./naivecycles-1)*100;
figure(1)
clf
bar(speedupwindow)
title('sspeedup');
set(gca,'XTick',1:30,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:30,'XTickLabel',benchmarks)

%%
FID = fopen('speeduptable.dat','w');
fprintf(FID,'X\tbenchmark\ttype\ts8\ts16\ts32\n');
x=0
for k = 1:30
    x = x+1;
    %prefill
    sp = roundn([speedupprefill(k,3),speedupprefill(k,2)-speedupprefill(k,3),speedupprefill(k,1)-speedupprefill(k,2)],-1);
    % window
    sw = roundn([speedupwindow(k,3),speedupwindow(k,2)-speedupwindow(k,3),speedupwindow(k,1)-speedupwindow(k,2)],-1);
    
    fprintf(FID,'%3.2d\t%s\tprefill\t%3.2d\t%3.2d\t%3.2d\n', x, benchmarks{k}, sp(1), sp(2), sp(3) );
    x=x+1;
    fprintf(FID,'%3.2d\t%s\twindow\t%3.2d\t%3.2d\t%3.2d\n', x, benchmarks{k}, sw(1), sw(2), sw(3) );
    x=x+1;
end

fclose(FID);

%%
FID = fopen('stackpercenttable.dat','w');
fprintf(FID, 'X\tname\tp\n');
x=0;
for k = 1:30
    x = x+1;
    fprintf(FID, '%3.2d\t%s\t%3.2d\n', k, benchmarks{k}, stackpercentage(k));
end
%%
FID = fopen('speedupnaive1KB.dat','w');
fprintf(FID, 'X\tname\tp\n');
x=0;
for k = 1:30
    x = x+1;
    fprintf(FID, '%3.2d\t%s\t%3.2d\n', k, benchmarks{k}, speedupnaive(k,1));
end

%%
FID = fopen('speedupprefill.dat', 'w');
fprintf(FID, 'X\tname\tb256\tb512\tb1024\n');
x = 0;
for k = 1:30
    x = x + 1;
    fprintf( FID, '%3.2d\t%s\t%3.2d\t%3.2d\t%3.2d\n', k, benchmarks{k}, speedupprefill(k,3),speedupprefill(k,2),speedupprefill(k,1));
end
%%
FID = fopen('speedupwindow.dat', 'w');
fprintf(FID, 'X\tname\tb256\tb512\tb1024\n');
x = 0;
for k = 1:30
    x = x + 1;
    fprintf( FID, '%3.2d\t%s\t%3.2d\t%3.2d\t%3.2d\n', k, benchmarks{k}, speedupwindow(k,3),speedupwindow(k,2),speedupwindow(k,1));
end
%%
[ocs32_32_4,ohs32_32_4,oshs32_32_4] = readarm('simple/armresults/original/dl1_64_32_4/',2);
%[ocs128_32_1,ohs128_32_1,oshs128_32_1] = readmibench('simple/armresults/orgstack/dl1_128_32_1/',2);
hitrate = [ohs32_32_4',ohs32_32_4',ohs32_32_4']*100;
orgcycles = [ocs32_32_4',ocs32_32_4',ocs32_32_4'];

[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1, stackpercentage] = readarm('simple/armresults/naive/dl1_32_32_4_sl1_128_32_1/',1);
%[nc32_32_4_16_32_1,nh32_32_2_16_32_1,nhs32_32_4_16_32_1] = readarm('simple/armresults/naive/dl1_32_32_4_sl1_16_32_1/',1);
%[nc32_32_4_8_32_1,nh32_32_4_8_32_1,nhs32_32_4_8_32_1] = readarm('simple/armresults/naive/dl1_32_32_4_sl1_8_32_1/',1);

%stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs32_32_4_8_32_1']*100;
%hitrate = [nh32_32_4_32_32_1',nh32_32_2_16_32_1',nh32_32_4_8_32_1']*100;
%naivecycles = [nc32_32_4_32_32_1',nc32_32_4_16_32_1',nc32_32_4_8_32_1'];
stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_32_32_1',nhs32_32_4_32_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_4_32_32_1',nh32_32_4_32_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc32_32_4_32_32_1',nc32_32_4_32_32_1'];
speeduporg = (naivecycles./orgcycles-1)*100;
%%
FID = fopen('speedupequalsize.dat','w');
fprintf(FID, 'X\tname\tp\n');
x=0;
for k = 1:30
    x = x+1;
    fprintf(FID, '%3.2d\t%s\t%3.2d\n', k, benchmarks{k}, speeduporg(k,1));
end

%%
FID = fopen('speedup8kb.dat','w');
fprintf(FID, 'X\tname\tp\n');
x=0;
for k = 1:30
    x = x+1;
    fprintf(FID, '%3.2d\t%s\t%3.2d\n', k, benchmarks{k}, kb8speedup(k,1));
end
