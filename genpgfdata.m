%% mibench
benchmarks = {'anagram','cc1','compress','go','perl','adpcm_decode','adpcm_encode','basicmath','blowfish_decode','blowfish_encode','CRC32','dijkstra','patricia','qsort','rijndael_decode','rijndael_encode','stringsearch','susan_corners','susan_edges','susan_smoothing'};

[ocs64_32_2,ohs64_32_2,oshs64_32_2,stackpercentage] = readall('simple/results/orgstack/dl1_64_32_2/',4);
[ocs32_32_4,ohs32_32_4,oshs32_32_4] = readall('simple/results/orgstack/dl1_32_32_4/',4);
[ocs128_32_1,ohs128_32_1,oshs128_32_1] = readmibench('simple/results/orgstack/dl1_128_32_1/',4);
[ocs64_32_4,ohs64_32_4,oshs64_32_4,stackpercentage] = readall('simple/results/orgstack/dl1_64_32_4/',4);

stackrate = [oshs32_32_4',oshs32_32_4',oshs32_32_4']*100;
hitrate = [ohs32_32_4',ohs32_32_4',ohs32_32_4']*100;
orgcycles = [ocs32_32_4',ocs32_32_4',ocs32_32_4'];
kb8cycles = [ocs64_32_4',ocs64_32_4',ocs64_32_4'];
kb8speedup = (orgcycles./kb8cycles-1)*100;
figure(1)
clf
bar(stackrate)
title('stackrate');
ylim([40,100]);
set(gca,'XTick',1:20,'XTickLabel',benchmarks)
figure(2)
clf
bar(kb8speedup)
title('hitrate');
%ylim([40,100]);
set(gca,'XTick',1:20,'XTickLabel',benchmarks)

%% naive mibench
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1] = readall('simple/results/naive/dl1_32_32_4_sl1_32_32_1/',1);
[nc32_32_4_16_32_1,nh32_32_2_16_32_1,nhs32_32_4_16_32_1] = readall('simple/results/naive/dl1_32_32_4_sl1_16_32_1/',1);
[nc32_32_4_8_32_1,nh32_32_4_8_32_1,nhs32_32_4_8_32_1] = readall('simple/results/naive/dl1_32_32_4_sl1_8_32_1/',1);

stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs32_32_4_8_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_2_16_32_1',nh32_32_4_8_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc32_32_4_16_32_1',nc32_32_4_8_32_1'];
speedupnaive = (orgcycles./naivecycles-1)*100;
figure(1)
clf
bar(stackrate)
title('stackrate');
ylim([40,100]);
set(gca,'XTick',1:20,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:20,'XTickLabel',benchmarks)

%%
figure(2)
bar(speedupnaive);
title('speedup');
set(gca,'XTick',1:20,'XTickLabel',benchmarks)

%% prefill mibench
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1] = readall('simple/results/tagless_prefillv2/dl1_32_32_4_sl1_32_32_1/',3);
[nc32_32_4_16_32_1,nh32_32_2_16_32_1,nhs32_32_4_16_32_1] = readall('simple/results/tagless_prefillv2/dl1_32_32_4_sl1_16_32_1/',3);
[nc32_32_4_8_32_1,nh32_32_4_8_32_1,nhs32_32_4_8_32_1] = readall('simple/results/tagless_prefillv2/dl1_32_32_4_sl1_8_32_1/',3);

stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs32_32_4_8_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_2_16_32_1',nh32_32_4_8_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc32_32_4_16_32_1',nc32_32_4_8_32_1'];
speedupprefill = (orgcycles./naivecycles-1)*100;
figure(1)
clf
bar(speedupprefill)
title('sspeedup');
set(gca,'XTick',1:20,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:20,'XTickLabel',benchmarks)

%% window mibench
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1] = readall('simple/results/window/dl1_32_32_4_sl1_32_32_1/',3);
[nc32_32_4_16_32_1,nh32_32_2_16_32_1,nhs32_32_4_16_32_1] = readall('simple/results/window/dl1_32_32_4_sl1_16_32_1/',3);
[nc32_32_4_8_32_1,nh32_32_4_8_32_1,nhs32_32_4_8_32_1] = readall('simple/results/window/dl1_32_32_4_sl1_8_32_1/',3);

stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs32_32_4_8_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_2_16_32_1',nh32_32_4_8_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc32_32_4_16_32_1',nc32_32_4_8_32_1'];
speedupwindow = (orgcycles./naivecycles-1)*100;
figure(1)
clf
bar(speedupwindow)
title('sspeedup');
set(gca,'XTick',1:20,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:20,'XTickLabel',benchmarks)

%% pgfplot datafile
FID = fopen('speeduptable.dat','w');
fprintf(FID,'X\tbenchmark\ttype\ts8\ts16\ts32\n');
x=0
for k = 1:20
    x = x+1;
    %prefill
    sp = roundn([speedupprefill(k,3),speedupprefill(k,2)-speedupprefill(k,3),speedupprefill(k,1)-speedupprefill(k,2)],-2);
    % window
    sw = roundn([speedupwindow(k,3),speedupwindow(k,2)-speedupwindow(k,3),speedupwindow(k,1)-speedupwindow(k,2)],-2);
    
    fprintf(FID,'%3.2d\t%s\tprefill\t%3.2d\t%3.2d\t%3.2d\n', x, benchmarks{k}, sp(1), sp(2), sp(3) );
    x=x+1;
    fprintf(FID,'%3.2d\t%s\twindow\t%3.2d\t%3.2d\t%3.2d\n', x, benchmarks{k}, sw(1), sw(2), sw(3) );
    x=x+1;
end

fclose(FID);