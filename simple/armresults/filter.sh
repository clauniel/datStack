#!/bin/bash

for d in */ ; do
	cd "$d";
	for d2 in */ ; do
		cd "$d2";
		for i in *.trace; do
			newfile="$(echo "$i" | sed "s/trace/txt/")";
			grep -e "sl1" -e "dl1" -e "sim_cyc" -e "stack_acc" -e "stack_miss" "$i" > "$newfile";
			sed -i "s/stack_/stack_1/" "$newfile";
			sed -i "s/sim_cycle/sim_cycle1/" "$newfile";
			sed -i "s/[^0-9]//g" "$newfile";
			sed -i "s/^.//" "$newfile";
		done;
		cd ".."
	done;
	cd ".."
done;
