#!/bin/sh
../../sim-outorder susan input_large.pgm output_large.smoothing.pgm -s 2> susan_large_smoothing.trace
../../sim-outorder susan input_large.pgm output_large.edges.pgm -e 2> susan_large_edges.trace
../../sim-outorder susan input_large.pgm output_large.corners.pgm -c 2> susan_large_corners.trace

