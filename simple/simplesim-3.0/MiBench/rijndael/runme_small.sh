#!/bin/sh
../../sim-outorder rijndael input_small.asc output_small.enc e 1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321 2> ../results/rijndael_small_encode.trace
../../sim-outorder rijndael output_small.enc output_small.dec d 1234567890abcdeffedcba09876543211234567890abcdeffedcba0987654321 2> ../results/rijndael_small_decode.trace

