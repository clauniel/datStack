#!/bin/sh
../../sim-outorder bin/rawcaudio < data/small.pcm > output_small.adpcm 2> ../results/adpcm_encode_small.trace
../../sim-outorder bin/rawdaudio < data/small.adpcm > output_small.pcm 2> ../results/adpcm_decode_small.trace
