#!/bin/sh
../../sim-outorder bf e input_small.asc output_small.enc 1234567890abcdeffedcba0987654321 2> ../results/blowfish_small_encode.trace
../../sim-outorder bf d output_small.enc output_small.asc 1234567890abcdeffedcba0987654321 2>  ../results/blowfish_small_decode.trace
