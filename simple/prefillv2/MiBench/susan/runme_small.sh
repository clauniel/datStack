#!/bin/sh
../../sim-outorder susan input_small.pgm output_small.smoothing.pgm -s 2> ../results/susan_small_smoothing.trace
../../sim-outorder susan input_small.pgm output_small.edges.pgm -e 2> ../results/susan_small_edges.trace
../../sim-outorder susan input_small.pgm output_small.corners.pgm -c 2> ../results/susan_small_corners.trace

