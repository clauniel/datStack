function [ cycles hitrate stackhitrate stackpercentage ] = readresults( dirpath, withstack )
%READRESULTS Summary of this function goes here
%   Detailed explanation goes here
anagram = fopen(strcat(dirpath,'anagram.txt'));
cc1 = fopen(strcat(dirpath,'cc1.txt'));
compress = fopen(strcat(dirpath,'compress95.txt'));
go = fopen(strcat(dirpath,'go.txt'));
perle = fopen(strcat(dirpath,'perl.txt'));

A = fscanf(anagram,'%f');
B = fscanf(cc1,'%f');
C = fscanf(compress,'%f');
D = fscanf(go,'%f');
E = fscanf(perle,'%f');

stackhitrate=[];
stackpercentage=[];

if withstack == 1
    cycles = [A(12),B(12),C(12),D(12),E(12)];
    hitrate = [(A(14)+A(25))/(A(13)+A(24)),(B(14)+B(25))/(B(13)+B(24)),(C(14)+C(25))/(C(13)+C(24)),(D(14)+D(25))/(D(13)+D(24)),(E(14)+E(25))/(E(13)+E(24))];
    stackhitrate = [A(25)/A(24),B(25)/B(24),C(25)/C(24),D(25)/D(24),E(25)/E(24)];
elseif withstack == 2
    cycles = [A(10),B(10),C(10),D(10),E(10)];
    hitrate = [A(12)/A(11),B(12)/B(11),C(12)/C(11),D(12)/D(11),E(12)/E(11)];
elseif withstack == 3
    cycles = [A(12),B(12),C(12),D(12),E(12)];
    hitrate = [(A(15)+A(26))/(A(14)+A(13)),(B(15)+B(26))/(B(14)+B(13)),(C(15)+C(26))/(C(14)+C(13)),(D(15)+D(26))/(D(14)+D(13)),(E(15)+E(26))/(E(14)+E(13))];
elseif withstack == 4
    cycles = [A(10),B(10),C(10),D(10),E(10)];
    hitrate = [A(14)/A(13),B(14)/B(13),C(14)/C(13),D(14)/D(13),E(14)/E(13)];
    stackhitrate = 1-[A(12)/A(11),B(12)/B(11),C(12)/C(11),D(12)/D(11),E(12)/E(11)]/10;
    stackpercentage = [A(11)/A(13),B(11)/B(13),C(11)/C(13),D(11)/D(13),E(11)/E(13)]
end

fclose(anagram);
fclose(cc1);
fclose(compress);
fclose(go);
fclose(perle);
