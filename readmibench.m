function [ cycles hitrate stackhitrate stackpercentage ] = readmibench( dirpath, withstack )
%READMIBENCH Summary of this function goes here
%   Detailed explanation goes here
filenames = {'adpcm_decode_small.txt','adpcm_encode_small.txt','basicmath_small.txt','blowfish_small_decode.txt','blowfish_small_encode.txt','CRC32_small.txt','dijkstra_small.txt','patricia_small.txt','qsort_small.txt','rijndael_small_decode.txt','rijndael_small_encode.txt','stringsearch_small.txt','susan_small_corners.txt','susan_small_edges.txt','susan_small_smoothing.txt'};

filehandles = [];

files = [];
for k = 1:length(filenames)
    filenames{k}
    filehandles = [filehandles,fopen(strcat(dirpath,filenames{k}))];
    files = [files,fscanf(filehandles(k), '%f')];
end

stackhitrate=[];
cycles = [];
hitrate = [];
stackpercentage= [];
for temp = files
    if withstack == 1
        cycles = [cycles,temp(12)];
        hitrate = [hitrate,(temp(14)+temp(25))/(temp(13)+temp(24))];
        stackhitrate = [stackhitrate,temp(25)/temp(24)];
    elseif withstack == 2
        cycles = [cycles, temp(10)];
        hitrate = [hitrate,temp(12)/temp(11)];
    elseif withstack == 3
        cycles = [cycles,temp(12)];
        hitrate = [hitrate,(temp(15)+temp(26))/(temp(14)+temp(13))];
    elseif withstack == 4
        cycles = [cycles,temp(10)];
        hitrate = [hitrate,temp(14)/temp(13)];
        stackhitrate = [stackhitrate,1-temp(12)/temp(11)/10];
        stackpercentage = [stackpercentage,temp(11)/temp(13)];
    end
end
for k = filehandles
    fclose(k);
end

end

