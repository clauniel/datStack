function [ cycles hitrate stackhitrate stackpercentage ] = readall( dirpath, withstack )
%READALL Summary of this function goes here
%   Detailed explanation goes here
[cycles, hitrate, stackhitrate, stackpercentage] = readresults( dirpath, withstack);
[temp1, temp2, temp3, temp4] = readmibench( dirpath, withstack );
cycles = [cycles,temp1];
hitrate = [hitrate, temp2];
stackhitrate = [stackhitrate, temp3];
stackpercentage = [stackpercentage,temp4];


end

