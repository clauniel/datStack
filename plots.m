%%
%silly matlab needs to have figures cleared
figHandles = findall(0,'Type','figure');
for i=1:length(figHandles)
    clf(figHandles(i));
end
figure(1)
labels={'anagram','cc1','compress','go','perl'};
%dl1:128:32:4:l
missrate0 = [(1-0.0056),(1-0.0095),(1-0.0384),(1-0.0086),(1-0.0060)];
%dl1:128:32:2:l
missrate1 = [(1-0.0149),(1-0.0303),(1-0.0742),(1-0.0290),(1-0.0178)];
%dl1:128:32:1:l
missrate2 = [(1-0.047),(1-0.0598),(1-0.0874),(1-0.0571),(1-0.0478)];
%dl1:512:32:1:l
missrate3 = [(1-0.0096),(1-0.0583),(1-0.1687),(1-0.0446),(1-0.0254)];
%dl1:256:32:2:l
missrate4 = [(1-0.0136),(1-0.0403),(1-0.0949),(1-0.0368),(1-0.0174)];
T = [missrate0; missrate1; missrate2; missrate3; missrate4]*100;
%Y=[(1-0.0056),(1-0.0095),(1-0.0384),(1-0.0086),(1-0.0060); 
%    (1-0.0149),(1-0.0303),(1-0.0742),(1-0.0290),(1-0.0178); 
%    (1-0.047),(1-0.0598),(1-0.0874),(1-0.0571),(1-0.0478); 
%    (1-0.0096),(1-0.0583),(1-0.1687),(1-0.0446),(1-0.0254); 
%    (1-0.0136),(1-0.0403),(1-0.0949),(1-0.0368),(1-0.0174)]
h1 = bar(T)
set(gca,'XTick',1:5,'XTickLabel',labels)
lh = legend('16kB:4','8kB:2','4kB:1','16kB:2','16kB:1','location','southoutside','orientation','horizontal');
title('l1 data cache hit rate');
set(lh,'PlotBoxAspectRatioMode','manual');
set(lh,'PlotBoxAspectRatio',[1 0.05 1]);
xlabel('Benchmark');
ylabel('Hit rate %');
ylim([50,100]);
set(gcf, 'paperunits', 'centimeters', 'PaperPosition',[0,0,10,4]);
saveas(gcf, './pics/orghitrate.eps','psc2');

memAccesses=[6058968;115494421;29738586;167959819;11369598]

figure(2)
stack8196=[1988626;66440088;2467408;54756076;3736079]
Y=stack8196./memAccesses*100;
set(gcf, 'paperunits', 'centimeters', 'PaperPosition',[0,0,10,4]);
bar(Y)
set(gca,'XTick',1:5,'XTickLabel',labels)
xlabel('Benchmark');
ylabel('Stack access ratio %');
title('Percentage of memory accesses directed to stack');
saveas(gcf, './pics/stackrate.eps','psc2');

%%
figure(3)
stack0=[1562899;59407647;1282417;52345296;2876966]
stack2048=[1988626;66440082;2467407;54756076;3736303]
stack1024=[1988626;66440072;2467407;54756076;3736303]
stack512=[1988626;66440037;2467407;54756076;3736303]
stack256=[1988626;66439661;2467407;54756076;3736303]
stack128=[1988626;66439580;2467407;54756076;3736303]
stack64=[1986897;66430617;2467407;54754716;3735897]
stack32=[1951010;63137684;2465549;52585925;3605486]
stack16=[1901388;61313741;2221130;52398172;3225499]
stack8=[1607419;59827054;1487359;52353408;2933508]
Y=[stack256./stack8196, stack128./stack8196, stack64./stack8196, stack32./stack8196, stack16./stack8196, stack8./stack8196, stack0./stack8196]
bar(Y)
set(gca,'XTick',1:5,'XTickLabel',labels)

figure(4)
stacko4096=[1986897;64543032;2467407;54754716;3725569]
stacko2048=[1904002;63226291;2454491;54740481;3690071]
stacko1024=[1904002;61176282;2454491;54740481;3577656]
stacko512=[1904002;59585438;2454491;54732993;3560962]
stacko256=[1903865;54836516;2454428;53921716;3508814]
stacko128=[1903556;49848232;2454356;42823888;3223082]
stacko64=[1651191;39586556;2454185;28192431;2869440]
stacko32=[1185668;15997913;2450017;10502206;1880546]
stacko16=[428692;7182780;1187319;3359921;882228]
stacko8=[426607;7163238;1187217;2964816;881495]
Y=[stacko4096./stack8196,stacko2048./stack8196,stacko1024./stack8196,stacko512./stack8196,stacko256./stack8196,stacko128./stack8196,stacko64./stack8196,stacko32./stack8196,stacko16./stack8196,stacko8./stack8196]
bar(Y)
set(gca,'XTick',1:5,'XTickLabel',labels)

figure(5)
Y=[(stack32+stacko512-stack8196)./stack8196,(stack32+stacko1024-stack8196)./stack8196,(stack16+stacko512-stack8196)./stack8196];
bar(Y)
set(gca,'XTick',1:5,'XTickLabel',labels)

figure(6)
%dl1:128:32:4:l, sl1:32:32:1:l
dl1asl1hits0 = [4036345+1988580,47610367+64784222,25875425+2467376,111577100+54716922,7497209+3714603];
%dl1:128:32:2:l
dl1asl1hits1 = [4036345+1988580,46621426+64853069,25513539+2467376,105432339+54812990,7303223+3717771];
%dl1:128:32:1:l
dl1asl1hits2 = [3913623+1988395,44070146+65001286,24733074+2467474,90825876+55059068,6941212+3722151];
%dl1:512:32:1:l
dl1asl1hits3 = [4020597+1988591,46797545+64825590,25581263+2467376,106215791+54784904,7289760+3715578];
%dl1:256:32:2:l
dl1asl1hits4 = [4035476+1988585,47432492+64797471,25851681+2467376,108957955+54754209,7447245+3714542];
withstack = [dl1asl1hits0;dl1asl1hits1;dl1asl1hits2;dl1asl1hits3;dl1asl1hits4]
missstack = bsxfun(@rdivide, withstack',memAccesses)
%missstack = [dl1asl1hits0./memAccesses';dl1asl1hits1./memAccesses';dl1asl1hits2./memAccesses';dl1asl1hits3./memAccesses';dl1asl1hits4./memAccesses']

Y = []
for n = 1:5
    Y = [Y,T(:,n),missstack(:,n)]
end
bar(Y)
set(gca,'XTick',1:5,'XTickLabel',labels)

figure(7)
%dl1:32_32_2 sl1:none
org32_2 = [5832428/6073246,106786298/116309545,27286376/29777009,134248569/170082020,10265055/11474595];
%dl1:64_32_1 sl1:none
org64_1 = [5698909/6080115,103617176/116530934,26382535/29736431,126884973/170491125,9941785/11476055];
%dl1:32:32:1 sl1:32:32:1 naive
naive = [(3609546+1988978)/(4102418+1989016),(39333082+65238161)/(49738161+66869352),(23075350+2468565)/(27250608+3468596),(66916500+55305339)/(115493596+55306517),(6119583+3740555)/(7733172+3762118)];
%dl1:32:32:1 sl1:32:32:1 prefill
prefill = [(3713593+1864394)/(4227811+1864394),(44558111+59392636)/(57230776+59418487),(23086685+2455551)/(27263623+2455551),(66930080+55290509)/(115509451+55290911),(6449744+3388900)/(8106630+3389051)];
bar([org32_2',org64_1',naive',prefill'])
set(gca,'XTick',1:5,'XTickLabel',labels)
%%
figure(8)
%numcycles
org32_2 = [9488794/9488794,316267473/316267473,49494316/49494316,685612110/685612110,32375369/32375369];
org64_1 = [9488794/9855279,316267473/322670615,49494316/51481094,685612110/697935987,32375369/32736768];
naive = [9488794/10288527,316267473/323410690,49494316/53515421,685612110/713911037,32375369/33247837];
prefill = [9488794/10330366,316267473/324859466,49494316/53517023,685612110/713895996,32375369/33261955];
bar([org32_2',org64_1',naive',prefill'])
set(gca,'XTick',1:5,'XTickLabel',labels)

%%
%read data
%naive stackcache
[nc128_32_4_32_32_1,nh128_32_4_32_32_1] = readresults('simple/results/naive/dl1_128_32_4_sl1_32_32_1/',1);
[nc128_32_2_32_32_1,nh128_32_2_32_32_1] = readresults('simple/results/naive/dl1_128_32_2_sl1_32_32_1/',1);
[nc128_32_1_32_32_1,nh128_32_1_32_32_1] = readresults('simple/results/naive/dl1_128_32_1_sl1_32_32_1/',1);
[nc256_32_2_32_32_1,nh256_32_2_32_32_1] = readresults('simple/results/naive/dl1_256_32_2_sl1_32_32_1/',1);
[nc512_32_1_32_32_1,nh512_32_1_32_32_1] = readresults('simple/results/naive/dl1_512_32_1_sl1_32_32_1/',1);
[nc32_32_1_32_32_1,nh32_32_1_32_32_1] = readresults('simple/results/naive/dl1_32_32_1_sl1_32_32_1/',1);
[nc64_32_1_32_32_1,nh32_32_1_32_32_1] = readresults('simple/results/naive/dl1_64_32_1_sl1_32_32_1/',1);
[nc32_32_1_16_32_1,nh32_32_1_16_32_1] = readresults('simple/results/naive/dl1_32_32_1_sl1_16_32_1/',1);
hnaive = [nh128_32_4_32_32_1',nh128_32_2_32_32_1',nh128_32_1_32_32_1',nh256_32_2_32_32_1',nh512_32_1_32_32_1',nh32_32_1_32_32_1',nh64_32_1_32_32_1'];
cnaive = [nc128_32_4_32_32_1',nc128_32_2_32_32_1',nc128_32_1_32_32_1',nc256_32_2_32_32_1',nc512_32_1_32_32_1',nc32_32_1_32_32_1',nc64_32_1_32_32_1'];
%prefill 
[pc128_32_4_32_32_1,ph128_32_4_32_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_4_sl1_32_32_1/',1);
[pc128_32_2_32_32_1,ph128_32_2_32_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_2_sl1_32_32_1/',1);
[pc128_32_1_32_32_1,ph128_32_1_32_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_1_sl1_32_32_1/',1);
[pc256_32_2_32_32_1,ph256_32_2_32_32_1] = readresults('simple/results/tagless_prefill/dl1_256_32_2_sl1_32_32_1/',1);
[pc512_32_1_32_32_1,ph512_32_1_32_32_1] = readresults('simple/results/tagless_prefill/dl1_512_32_1_sl1_32_32_1/',1);
[pc32_32_1_32_32_1,ph32_32_1_32_32_1] = readresults('simple/results/tagless_prefill/dl1_32_32_1_sl1_32_32_1/',1);
[pc64_32_1_32_32_1,ph32_32_1_32_32_1] = readresults('simple/results/tagless_prefill/dl1_64_32_1_sl1_32_32_1/',1);
[pc32_32_1_16_32_1,ph32_32_1_16_32_1] = readresults('simple/results/tagless_prefill/dl1_32_32_1_sl1_16_32_1/',1);
hprefill = [ph128_32_4_32_32_1',ph128_32_2_32_32_1',ph128_32_1_32_32_1',ph256_32_2_32_32_1',ph512_32_1_32_32_1',ph32_32_1_32_32_1',ph64_32_1_32_32_1'];
cprefill = [pc128_32_4_32_32_1',pc128_32_2_32_32_1',pc128_32_1_32_32_1',pc256_32_2_32_32_1',pc512_32_1_32_32_1',pc32_32_1_32_32_1',pc64_32_1_32_32_1'];


%original
[oc128_32_4,oh128_32_4] = readresults('simple/results/original/dl1_128_32_4/',0);
[oc128_32_2,oh128_32_2] = readresults('simple/results/original/dl1_128_32_2/',0);
[oc128_32_1,oh128_32_1] = readresults('simple/results/original/dl1_128_32_1/',0);
[oc256_32_2,oh256_32_2] = readresults('simple/results/original/dl1_256_32_2/',0);
[oc512_32_1,oh512_32_1] = readresults('simple/results/original/dl1_512_32_1/',0);
[oc32_32_2,oh32_32_2] = readresults('simple/results/original/dl1_32_32_2/',0);
[oc64_32_1,oh64_32_1] = readresults('simple/results/original/dl1_64_32_1/',0);
horg = [oh128_32_4',oh128_32_2',oh128_32_1',oh256_32_2',oh512_32_1',oh32_32_2',oh64_32_1'];
corg = [oc128_32_4', oc128_32_2', oc128_32_1', oc256_32_2', oc512_32_1', oc32_32_2', oc64_32_1'];

corgs = corg./corg;
cprefills = cprefill./corg;
cnaives = cnaive./cnaive;
%% coherent stuff with all accesses offset from stackpointer
%read data
%naive stackache
[nc128_32_4_32_32_1,nh128_32_4_32_32_1] = readresults('simple/results/naive/dl1_128_32_4_sl1_32_32_1/',1);
[nc128_32_2_32_32_1,nh128_32_2_32_32_1] = readresults('simple/results/naive/dl1_128_32_2_sl1_32_32_1/',1);
[nc128_32_1_32_32_1,nh128_32_1_32_32_1] = readresults('simple/results/naive/dl1_128_32_1_sl1_32_32_1/',1);
[nc256_32_2_32_32_1,nh256_32_2_32_32_1] = readresults('simple/results/naive/dl1_256_32_2_sl1_32_32_1/',1);
[nc512_32_1_32_32_1,nh512_32_1_32_32_1] = readresults('simple/results/naive/dl1_512_32_1_sl1_32_32_1/',1);
[nc128_32_4_64_32_1,nh128_32_4_64_32_1] = readresults('simple/results/naive/dl1_128_32_4_sl1_64_32_1/',1);
[nc128_32_2_64_32_1,nh128_32_2_64_32_1] = readresults('simple/results/naive/dl1_128_32_2_sl1_64_32_1/',1);
[nc128_32_1_64_32_1,nh128_32_1_64_32_1] = readresults('simple/results/naive/dl1_128_32_1_sl1_64_32_1/',1);
[nc256_32_2_64_32_1,nh256_32_2_64_32_1] = readresults('simple/results/naive/dl1_256_32_2_sl1_64_32_1/',1);
[nc512_32_1_64_32_1,nh512_32_1_64_32_1] = readresults('simple/results/naive/dl1_512_32_1_sl1_64_32_1/',1);
[nc128_32_4_16_32_1,nh128_32_4_16_32_1] = readresults('simple/results/naive/dl1_128_32_4_sl1_16_32_1/',1);
[nc128_32_2_16_32_1,nh128_32_2_16_32_1] = readresults('simple/results/naive/dl1_128_32_2_sl1_16_32_1/',1);
[nc128_32_1_16_32_1,nh128_32_1_16_32_1] = readresults('simple/results/naive/dl1_128_32_1_sl1_16_32_1/',1);
[nc256_32_2_16_32_1,nh256_32_2_16_32_1] = readresults('simple/results/naive/dl1_256_32_2_sl1_16_32_1/',1);
[nc512_32_1_16_32_1,nh512_32_1_16_32_1] = readresults('simple/results/naive/dl1_512_32_1_sl1_16_32_1/',1);
[nc32_32_1_32_32_1,nh32_32_1_32_32_1] = readresults('simple/results/naive/dl1_32_32_1_sl1_32_32_1/',1);
hnaive = [nh128_32_4_32_32_1',nh128_32_2_32_32_1',nh128_32_1_32_32_1',nh256_32_2_32_32_1',nh512_32_1_32_32_1',nh32_32_1_32_32_1'];
cnaive = [nc128_32_4_16_32_1',nc128_32_4_32_32_1',nc128_32_4_64_32_1',nc128_32_2_16_32_1',nc128_32_2_32_32_1',nc128_32_2_64_32_1',nc128_32_1_16_32_1',nc128_32_1_32_32_1',nc128_32_1_64_32_1',nc256_32_2_16_32_1',nc256_32_2_32_32_1',nc256_32_2_64_32_1',nc512_32_1_16_32_1',nc512_32_1_32_32_1',nc512_32_1_64_32_1'];
% prefill without tag
[pc128_32_4_32_32_1,ph128_32_4_32_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_4_sl1_32_32_1/',3);
[pc128_32_2_32_32_1,ph128_32_2_32_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_2_sl1_32_32_1/',3);
[pc128_32_1_32_32_1,ph128_32_1_32_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_1_sl1_32_32_1/',3);
[pc256_32_2_32_32_1,ph256_32_2_32_32_1] = readresults('simple/results/tagless_prefill/dl1_256_32_2_sl1_32_32_1/',3);
[pc512_32_1_32_32_1,ph512_32_1_32_32_1] = readresults('simple/results/tagless_prefill/dl1_512_32_1_sl1_32_32_1/',3);
[pc128_32_4_64_32_1,ph128_32_4_64_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_4_sl1_64_32_1/',3);
[pc128_32_2_64_32_1,ph128_32_2_64_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_2_sl1_64_32_1/',3);
[pc128_32_1_64_32_1,ph128_32_1_64_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_1_sl1_64_32_1/',3);
[pc256_32_2_64_32_1,ph256_32_2_64_32_1] = readresults('simple/results/tagless_prefill/dl1_256_32_2_sl1_64_32_1/',3);
[pc512_32_1_64_32_1,ph512_32_1_64_32_1] = readresults('simple/results/tagless_prefill/dl1_512_32_1_sl1_64_32_1/',3);
[pc128_32_4_16_32_1,ph128_32_4_16_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_4_sl1_16_32_1/',3);
[pc128_32_2_16_32_1,ph128_32_2_16_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_2_sl1_16_32_1/',3);
[pc128_32_1_16_32_1,ph128_32_1_16_32_1] = readresults('simple/results/tagless_prefill/dl1_128_32_1_sl1_16_32_1/',3);
[pc256_32_2_16_32_1,ph256_32_2_16_32_1] = readresults('simple/results/tagless_prefill/dl1_256_32_2_sl1_16_32_1/',3);
[pc512_32_1_16_32_1,ph512_32_1_16_32_1] = readresults('simple/results/tagless_prefill/dl1_512_32_1_sl1_16_32_1/',3);
[pc32_32_1_32_32_1,ph32_32_1_32_32_1] = readresults('simple/results/tagless_prefill/dl1_32_32_1_sl1_32_32_1/',3);
hprefill = [ph128_32_4_32_32_1',ph128_32_2_32_32_1',ph128_32_1_32_32_1',ph256_32_2_32_32_1',ph512_32_1_32_32_1',ph32_32_1_32_32_1'];
cprefill = [pc128_32_4_16_32_1',pc128_32_4_32_32_1',pc128_32_4_64_32_1',pc128_32_2_16_32_1',pc128_32_2_32_32_1',pc128_32_2_64_32_1',pc128_32_1_16_32_1',pc128_32_1_32_32_1',pc128_32_1_64_32_1',pc256_32_2_16_32_1',pc256_32_2_32_32_1',pc256_32_2_64_32_1',pc512_32_1_16_32_1',pc512_32_1_32_32_1',pc512_32_1_64_32_1'];

%prefill with tag
%prefill without tag
[pwc128_32_4_32_32_1,pwh128_32_4_32_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_4_sl1_32_32_1/',3);
[pwc128_32_2_32_32_1,pwh128_32_2_32_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_2_sl1_32_32_1/',3);
[pwc128_32_1_32_32_1,pwh128_32_1_32_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_1_sl1_32_32_1/',3);
[pwc256_32_2_32_32_1,pwh256_32_2_32_32_1] = readresults('simple/results/tag_prefill/dl1_256_32_2_sl1_32_32_1/',3);
[pwc512_32_1_32_32_1,pwh512_32_1_32_32_1] = readresults('simple/results/tag_prefill/dl1_512_32_1_sl1_32_32_1/',3);
[pwc128_32_4_64_32_1,pwh128_32_4_64_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_4_sl1_64_32_1/',3);
[pwc128_32_2_64_32_1,pwh128_32_2_64_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_2_sl1_64_32_1/',3);
[pwc128_32_1_64_32_1,pwh128_32_1_64_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_1_sl1_64_32_1/',3);
[pwc256_32_2_64_32_1,pwh256_32_2_64_32_1] = readresults('simple/results/tag_prefill/dl1_256_32_2_sl1_64_32_1/',3);
[pwc512_32_1_64_32_1,pwh512_32_1_64_32_1] = readresults('simple/results/tag_prefill/dl1_512_32_1_sl1_64_32_1/',3);
[pwc128_32_4_16_32_1,pwh128_32_4_16_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_4_sl1_16_32_1/',3);
[pwc128_32_2_16_32_1,pwh128_32_2_16_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_2_sl1_16_32_1/',3);
[pwc128_32_1_16_32_1,pwh128_32_1_16_32_1] = readresults('simple/results/tag_prefill/dl1_128_32_1_sl1_16_32_1/',3);
[pwc256_32_2_16_32_1,pwh256_32_2_16_32_1] = readresults('simple/results/tag_prefill/dl1_256_32_2_sl1_16_32_1/',3);
[pwc512_32_1_16_32_1,pwh512_32_1_16_32_1] = readresults('simple/results/tag_prefill/dl1_512_32_1_sl1_16_32_1/',3);
[pwc32_32_1_32_32_1,pwh32_32_1_32_32_1] = readresults('simple/results/tag_prefill/dl1_32_32_1_sl1_32_32_1/',3);
hwprefill = [pwh128_32_4_32_32_1',pwh128_32_2_32_32_1',pwh128_32_1_32_32_1',pwh256_32_2_32_32_1',pwh512_32_1_32_32_1',pwh32_32_1_32_32_1'];
cwprefill = [pwc128_32_4_16_32_1',pwc128_32_4_32_32_1',pwc128_32_4_64_32_1',pwc128_32_2_16_32_1',pwc128_32_2_32_32_1',pwc128_32_2_64_32_1',pwc128_32_1_16_32_1',pwc128_32_1_32_32_1',pwc128_32_1_64_32_1',pwc256_32_2_16_32_1',pwc256_32_2_32_32_1',pwc256_32_2_64_32_1',pwc512_32_1_16_32_1',pwc512_32_1_32_32_1',pwc512_32_1_64_32_1'];

%window
[wc128_32_4_32_32_1,wh128_32_4_32_32_1] = readresults('simple/results/window/dl1_128_32_4_32_32_1/',3);
[wc128_32_2_32_32_1,wh128_32_2_32_32_1] = readresults('simple/results/window/dl1_128_32_2_32_32_1/',3);
[wc128_32_1_32_32_1,wh128_32_1_32_32_1] = readresults('simple/results/window/dl1_128_32_1_32_32_1/',3);
[wc256_32_2_32_32_1,wh256_32_2_32_32_1] = readresults('simple/results/window/dl1_256_32_2_32_32_1/',3);
[wc512_32_1_32_32_1,wh512_32_1_32_32_1] = readresults('simple/results/window/dl1_512_32_1_32_32_1/',3);
[wc128_32_4_64_32_1,wh128_32_4_64_32_1] = readresults('simple/results/window/dl1_128_32_4_64_32_1/',3);
[wc128_32_2_64_32_1,wh128_32_2_64_32_1] = readresults('simple/results/window/dl1_128_32_2_64_32_1/',3);
[wc128_32_1_64_32_1,wh128_32_1_64_32_1] = readresults('simple/results/window/dl1_128_32_1_64_32_1/',3);
[wc256_32_2_64_32_1,wh256_32_2_64_32_1] = readresults('simple/results/window/dl1_256_32_2_64_32_1/',3);
[wc512_32_1_64_32_1,wh512_32_1_64_32_1] = readresults('simple/results/window/dl1_512_32_1_64_32_1/',3);
[wc128_32_4_16_32_1,wh128_32_4_16_32_1] = readresults('simple/results/window/dl1_128_32_4_16_32_1/',3);
[wc128_32_2_16_32_1,wh128_32_2_16_32_1] = readresults('simple/results/window/dl1_128_32_2_16_32_1/',3);
[wc128_32_1_16_32_1,wh128_32_1_16_32_1] = readresults('simple/results/window/dl1_128_32_1_16_32_1/',3);
[wc256_32_2_16_32_1,wh256_32_2_16_32_1] = readresults('simple/results/window/dl1_256_32_2_16_32_1/',3);
[wc512_32_1_16_32_1,wh512_32_1_16_32_1] = readresults('simple/results/window/dl1_512_32_1_16_32_1/',3);

cwindow = [wc128_32_4_16_32_1',wc128_32_4_32_32_1',wc128_32_4_64_32_1',wc128_32_2_16_32_1',wc128_32_2_32_32_1',wc128_32_2_64_32_1',wc128_32_1_16_32_1',wc128_32_1_32_32_1',wc128_32_1_64_32_1',wc256_32_2_16_32_1',wc256_32_2_32_32_1',wc256_32_2_64_32_1',wc512_32_1_16_32_1',wc512_32_1_32_32_1',wc512_32_1_64_32_1'];

%original
[oc128_32_4,oh128_32_4] = readresults('simple/results/original/dl1_128_32_4/',2);
[oc128_32_2,oh128_32_2] = readresults('simple/results/original/dl1_128_32_2/',2);
[oc128_32_1,oh128_32_1] = readresults('simple/results/original/dl1_128_32_1/',2);
[oc256_32_2,oh256_32_2] = readresults('simple/results/original/dl1_256_32_2/',2);
[oc512_32_1,oh512_32_1] = readresults('simple/results/original/dl1_512_32_1/',2);
[oc32_32_2,oh32_32_2] = readresults('simple/results/original/dl1_32_32_2/',2);
[oc64_32_1,oh64_32_1] = readresults('simple/results/original/dl1_64_32_1/',2);
horg = [oh128_32_4',oh128_32_2',oh128_32_1',oh256_32_2',oh512_32_1',oh32_32_2',oh64_32_1'];
corg = [oc128_32_4',oc128_32_4',oc128_32_4', oc128_32_2',oc128_32_2',oc128_32_2', oc128_32_1',oc128_32_1',oc128_32_1', oc256_32_2',oc256_32_2',oc256_32_2', oc512_32_1',oc512_32_1',oc512_32_1'];

corgs = (corg./corg-1)*100;
cprefills = (corg./cprefill-1)*100;
cnaives = (corg./cnaive-1)*100;
cwprefills = (corg./cwprefill-1)*100;
cwindows = (corg./cwindow -1)*100;

labels={'anagram','cc1','compress','go','perl'};

Y = [];
for i = 1:6;
    Y = [Y,horg(:,i),hnaive(:,i),hprefill(:,i),hwprefill(:,i)];
end

%figure(1)
%bar(Y)
%set(gca,'XTick',1:5,'XTickLabel',labels)


figure(2)
T = [];
for i = 1:15;
    T = [T,corgs(:,i),cnaives(:,i),cprefills(:,i),cwprefills(:,i),cwindows(:,i)];
end
bar(T)
set(gca,'XTick',1:5,'XTickLabel',labels)
ylim([-2,2])

%prettier plot?
cdl1_128_32_4 = [cnaives(:,1),cnaives(:,2),cnaives(:,3),cprefills(:,1),cprefills(:,2),cprefills(:,3),cwprefills(:,1),cwprefills(:,2),cwprefills(:,3),cwindows(:,1),cwindows(:,2),cwindows(:,3),];
C = cell(5,1);
for i = 1:5
    C{i} = [cnaives(:,i*3-2),cprefills(:,i*3-2),cwprefills(:,i*3-2),cwindows(:,i*3-2),cnaives(:,i*3-1),cprefills(:,i*3-1),cwprefills(:,i*3-1),cwindows(:,i*3-1),cnaives(:,i*3),cprefills(:,i*3),cwprefills(:,i*3),cwindows(:,i*3),];
end
figure(1)
clf(1)

title_str = {'d1$ 16kB 128:32:4','d1$ 8kB 128:32:2','d1$ 4kB 128:32:1','d1$ 16kB 256:32:2','d1$ 16kB 512:32:1'}
for i = 1:5
subplot(3,2,i)

b = bar(C{i});
set(gca,'XTick',1:5,'XTickLabel',labels)
ylim([-0.3,0.3]);
title(title_str{i});
ylabel('Speedup %');
end

h2 = subplot(3,2,6)
axis off;
lstr = {'512B naive','512B pre-tag','512B pre+tag', '512B window','1kB naive','1kB pre-tag','1kB pre+tag','1kB window','2kB naive','2kB pre-tag','2kB pre+tag','2kB window'};
h1 = gridLegend(b,2,lstr,'location','SouthOutside', 'FontSize', 8);
p = get(h1,'Position');
p2 = get(h2, 'Position');
p(1) = p2(1)+0.05;
p(2) = p2(2)-0.2;
p(3) = 0.2
set(h1,'Position',p)
subplot(3,2,1)
ylim([-0.15,0.15]);
subplot(3,2,2)
ylim([-0.2,0.5]);
subplot(3,2,3)
ylim([-0.1,1.8]);
subplot(3,2,4)
ylim([-0.2,0.12]);
subplot(3,2,5);
ylim([-0.2,0.4]);

set(gcf, 'paperunits', 'centimeters', 'PaperPosition', [0,0,18,15]);
saveas(gcf, 'pics/full.jpg','jpg');

%%
labels={'anagram','cc1','compress','go','perl'};
figure(2)
[ocs128_32_4,ohs128_32_4,oshs128_32_4] = readresults('simple/results/orgstack/dl1_128_32_4/',4);
[ocs128_32_2,ohs128_32_2,oshs128_32_2] = readresults('simple/results/orgstack/dl1_128_32_2/',4);
[ocs128_32_1,ohs128_32_1,oshs128_32_1] = readresults('simple/results/orgstack/dl1_128_32_1/',4);
[ocs256_32_2,ohs256_32_2,oshs256_32_2] = readresults('simple/results/orgstack/dl1_256_32_2/',4);
[ocs512_32_1,ohs512_32_1,oshs512_32_1] = readresults('simple/results/orgstack/dl1_512_32_1/',4);
[ocs64_32_2,ohs64_32_2,oshs64_32_2] = readresults('simple/results/orgstack/dl1_64_32_2/',4);
[ocs32_32_4,ohs32_32_4,oshs32_32_4] = readresults('simple/results/orgstack/dl1_32_32_4/',4);
orgstackrate = [oshs32_32_4',oshs64_32_2',oshs128_32_1']*100;
orghitrate = [ohs32_32_4',ohs64_32_2',ohs128_32_1']*100;
bar(orgstackrate);
ylim([80,100]);
title('4kB d1 stack access hit rate');
%legend('4 way','2 way', '1 way','location','southoutside','orientation','horizontal');
set(gca,'XTick',1:5,'XTickLabel',labels)
ylabel('Hit rate %');
xlabel('Benchmark');
set(gcf, 'paperunits', 'centimeters', 'PaperPosition',[0,0,10,4]);
saveas(gcf, './pics/orgstackhitrate.eps','psc2');
figure(1)
clf(1)
bar(orghitrate)
ylim([80,100]);
title('4kB d1 hit rate');
legend('4 way','2 way', '1 way','location','southoutside','orientation','horizontal');
ylabel('Hit rate %');
xlabel('Benchmark');

set(gca,'XTick',1:5,'XTickLabel',labels)
set(gcf, 'paperunits', 'centimeters', 'PaperPosition',[0,0,10,4]);
saveas(gcf, './pics/orghitratereduce.eps','psc2');

%%
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1] = readresults('simple/results/naive/dl1_32_32_4_sl1_32_32_1/',4);
[nc64_32_2_32_32_1,nh64_32_2_32_32_1,nhs64_32_2_32_32_1] = readresults('simple/results/naive/dl1_64_32_2_sl1_32_32_1/',4);
[nc128_32_1_32_32_1,nh128_32_1_32_32_1,nhs128_32_1_32_32_1] = readresults('simple/results/naive/dl1_128_32_1_sl1_32_32_1/',4);
[nc32_32_4_16_32_1,nh32_32_4_16_32_1,nhs32_32_4_16_32_1] = readresults('simple/results/naive/dl1_32_32_4_sl1_16_32_1/',4);
[nc64_32_2_16_32_1,nh64_32_2_16_32_1,nhs64_32_2_16_32_1] = readresults('simple/results/naive/dl1_64_32_2_sl1_16_32_1/',4);
[nc128_32_1_16_32_1,nh128_32_1_16_32_1,nhs128_32_1_16_32_1] = readresults('simple/results/naive/dl1_128_32_1_sl1_16_32_1/',4);

stackrate = [nhs32_32_4_32_32_1',nhs32_32_4_16_32_1',nhs64_32_2_32_32_1',nhs64_32_2_16_32_1',nhs128_32_1_32_32_1',nhs128_32_1_16_32_1',]*100;
hitrate = [nh32_32_4_32_32_1',nh32_32_4_16_32_1',nh64_32_2_32_32_1',nh64_32_2_16_32_1',nh128_32_1_32_32_1',nh128_32_1_16_32_1',]*100;
figure(1)
clf
bar(stackrate)
figure(2)
clf
bar(hitrate)

%% mibench
benchmarks = {'adpcm_decode','adpcm_encode','basicmath','blowfish_decode','blowfish_encode','CRC32','dijkstra','patricia','qsort','rijndael_decode','rijndael_encode','stringsearch','susan_corners','susan_edges','susan_smoothing'};

[ocs64_32_2,ohs64_32_2,oshs64_32_2] = readmibench('simple/results/orgstack/dl1_64_32_2/',4);
[ocs32_32_4,ohs32_32_4,oshs32_32_4] = readmibench('simple/results/orgstack/dl1_32_32_4/',4);
[ocs128_32_1,ohs128_32_1,oshs128_32_1] = readmibench('simple/results/orgstack/dl1_128_32_1/',4);
stackrate = [oshs32_32_4',oshs64_32_2',oshs128_32_1']*100;
hitrate = [ohs32_32_4',ohs64_32_2',ohs128_32_1']*100;
orgcycles = [ocs32_32_4',ocs64_32_2',ocs128_32_1'];
figure(1)
clf
bar(stackrate)
title('stackrate');
ylim([40,100]);
set(gca,'XTick',1:15,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:15,'XTickLabel',benchmarks)

%% naive mibench
[nc32_32_4_32_32_1,nh32_32_4_32_32_1,nhs32_32_4_32_32_1] = readmibench('simple/results/naive/dl1_32_32_4_sl1_32_32_1/',1);
[nc64_32_2_32_32_1,nh64_32_2_32_32_1,nhs64_32_2_32_32_1] = readmibench('simple/results/naive/dl1_64_32_2_sl1_32_32_1/',1);
[nc128_32_1_32_32_1,nh128_32_1_32_32_1,nhs128_32_1_32_32_1] = readmibench('simple/results/naive/dl1_128_32_1_sl1_32_32_1/',1);

stackrate = [nhs32_32_4_32_32_1',nhs64_32_2_32_32_1',nhs128_32_1_32_32_1']*100;
hitrate = [nh32_32_4_32_32_1',nh64_32_2_32_32_1',nh128_32_1_32_32_1']*100;
naivecycles = [nc32_32_4_32_32_1',nc64_32_2_32_32_1',nc128_32_1_32_32_1'];
speedup = orgcycles./naivecycles-1;
figure(1)
clf
bar(stackrate)
title('stackrate');
ylim([40,100]);
set(gca,'XTick',1:15,'XTickLabel',benchmarks)
figure(2)
clf
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:15,'XTickLabel',benchmarks)

%%
figure(2)
bar(speedup);
title('speedup');
set(gca,'XTick',1:15,'XTickLabel',benchmarks)

%% mibench prefill without tag
[pc32_32_4_32_32_1,ph32_32_4_32_32_1,phs32_32_4_32_32_1] = readmibench('simple/results/tagless_prefillv2/dl1_32_32_4_sl1_32_32_1/',3);
[pc64_32_2_32_32_1,ph64_32_2_32_32_1,phs64_32_2_32_32_1] = readmibench('simple/results/tagless_prefillv2/dl1_64_32_2_sl1_32_32_1/',3);
[pc128_32_1_32_32_1,ph128_32_1_32_32_1,phs128_32_1_32_32_1] = readmibench('simple/results/tagless_prefillv2/dl1_128_32_1_sl1_32_32_1/',3);

hitrate = [ph32_32_4_32_32_1',ph64_32_2_32_32_1',ph128_32_1_32_32_1']*100;
prefillcycles = [pc32_32_4_32_32_1',pc64_32_2_32_32_1',pc128_32_1_32_32_1'];

speedup = (orgcycles./prefillcycles-1)*100;

figure(1)
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:15,'XTickLabel',benchmarks)
figure(2)
clf
bar(speedup)
title('speedup');
set(gca,'XTick',1:15,'XTickLabel',benchmarks)

%%
[pc32_32_4_32_32_1,ph32_32_4_32_32_1,phs32_32_4_32_32_1] = readmibench('simple/results/window/dl1_32_32_4_sl1_32_32_1/',3);
[pc64_32_2_32_32_1,ph64_32_2_32_32_1,phs64_32_2_32_32_1] = readmibench('simple/results/window/dl1_64_32_2_sl1_32_32_1/',3);
[pc128_32_1_32_32_1,ph128_32_1_32_32_1,phs128_32_1_32_32_1] = readmibench('simple/results/window/dl1_128_32_1_sl1_32_32_1/',3);

hitrate = [ph32_32_4_32_32_1',ph64_32_2_32_32_1',ph128_32_1_32_32_1']*100;
prefillcycles = [pc32_32_4_32_32_1',pc64_32_2_32_32_1',pc128_32_1_32_32_1'];

speedup = (orgcycles./prefillcycles-1)*100;

figure(1)
bar(hitrate)
title('hitrate');
ylim([40,100]);
set(gca,'XTick',1:15,'XTickLabel',benchmarks)
figure(2)
clf
bar(speedup)
title('speedup');
set(gca,'XTick',1:15,'XTickLabel',benchmarks)