function [ cycles hitrate stackhitrate stackpercentage ] = readarm( dirpath, archtype )
%READARM Summary of this function goes here
%   Detailed explanation goes here
filenames = {'adpcm_decode.txt', 'adpcm_encode.txt','basicmath_small.txt','bitcnts.txt','blowfish_decode.txt','blowfish_encode.txt','CRC32.txt','dijkstra.txt','fft.txt','fft_inv.txt','ghostscript.txt','gsm_decode.txt','ispell.txt','jpeg_decode.txt','jpeg_encode.txt','lame.txt','patricia.txt','qsort_small.txt','rijndael_decode.txt','rijndael_encode.txt','rsynth.txt','sha.txt','stringsearch.txt','susan_corners.txt','susan_edges.txt','susan_smoothing.txt','tiff2bw.txt','tiff2rgba.txt','tiffdither.txt','tiffmedian.txt'}
filehandles = [];

files = [];
for k = 1:length(filenames)
    filenames{k}
    filehandles = [filehandles,fopen(strcat(dirpath,filenames{k}))];
    files = [files,fscanf(filehandles(k), '%f')];
end

stackhitrate=[];
cycles = [];
hitrate = [];
stackpercentage= [];
for temp = files
    if  archtype == 1 %naive
        cycles = [cycles,temp(12)];
        hitrate = [hitrate,(temp(14)+temp(24))/(temp(13)+temp(23))];
        stackpercentage = [stackpercentage,temp(23)/(temp(23)+temp(13))];
        stackhitrate = [stackhitrate, temp(24)/temp(23)];
    elseif archtype == 2 %original
        cycles = [cycles, temp(10)];
        hitrate = [hitrate,temp(12)/temp(11)];
    elseif archtype == 3 % others
        cycles = [cycles,temp(12)];
        hitrate = [hitrate,(temp(15)+temp(25))/(temp(14)+temp(24))];
        stackhitrate = [stackhitrate, temp(25)/temp(24)];
    end
end
for k = filehandles
    fclose(k);
end

end

