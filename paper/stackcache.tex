\section{Stack Cache}\label{stackcache}
We have implemented a simple stack cache and tested it in conjuction with the previosly mentioned MIPS processor. The only 
design constraint being that no compiler support must be needed so that existing tools can benefit from the implementation. 
This section describes the cache design and implementation.

\subsection{Design}
The MIPS register convention specifies that register 29 is used for a program's stack pointer and register 30 for the frame pointer.
Furthermore, stacks usually start at the top of memory and grow down by convention. Memory accesses are always offset from the stack
pointer or frame pointer. Our stack cache is a direct mapped cache that acts as a window into main memory. The window 
is bounded by two pointers, the top of stack (\emph{tos}) and bottom of stack (\emph{bos}) pointers. 

All memory accesses that lie between these 
two addresses will be directed to the stack cache. Memory accesses to addresses outside of this range will be directed to a separate 
data L1 cache, or the next level in the memory hierarchy if one is not present. This results in stack items competing for space with data items in the L1 cache if accesses are made 
to a memory area above the bottom of stack pointer (\emph{tos} is smaller than \emph{bos} because the stack grows downwards by convention).

The window tracks the movement of the stack pointer  within a range of the available address space, defined to be the 
area in which the stack can grow. When the stack pointer reaches a smaller value than the \emph{tos} pointer, \emph{tos} is adjusted to match the 
stack pointer and \emph{bos} is adjusted so it is a constant distance from \emph{tos}. Similarly, when the stack pointer becomes greater than \emph{bos},
\emph{bos} is adjusted to equal the stack pointer and \emph{tos} is adjusted to make the window a constant size. When the stack pointer moves 
between \emph{bos} and \emph{tos}, the window does not move. 

This means that stack loads/stores in a function will never miss as long as the maxmimum stack depth 
from the entry point of the function is smaller than the cache size. This is of course unlikely to be true for a lot of functions, but 
we have no numbers detailing exactly how often this is the case for various cache sizes and programs. Since the stack is almost always 
written to, when the stack pointer is decremented, it is a certainty that any item in the stack cache is overwritten when the stack pointer
is exactly the number of items in the cache away from the specific item. At this point the item will need to be evicted to either an 
L2 cache which holds both data and stack items, or main memory.

When the stack pointer returns to an area of memory after having been away by more than a cache size, items will need to be loaded back
into the stack cache. This could be accomplished by an automatic fill unit since the exact amount of items that need to be loaded 
can be calculated from difference between the stack pointer and the \emph{tos}/\emph{bos} pointer. A prefill unit that looks ahead in the instruction 
stream could also be constructed such that items would be ready in the cache upon the return from a function.

We opted against these options for ease of implementation. Our cache functions as a normal direct mapped cache that covers a variable 
area of memory and thus a program will need to miss in the stack before an item is loaded.

\subsection{Implementation}
We implemented the stack cache in the Chisel language together with the MIPS processor discussed earlier. We have not implemented a 
data cache so any memory access attempt that does not hit in the stack cache will go straight to main memory. Our policy for writes 
is write-through. We need to use tag memory for the cache as we do not have a fill unit that could automatically figure out which 
entries in the cache are invalidated upon a stack pointer jump of more than one address. A small cache controller with three states:
idle, read memory and write, can deliver same cycle results when a request hits in the cache. The delay on a miss 2 cycles
in addition to the delay of the level in the memory hierarchy where the request hits. For a write the delay is 2 cycles in addition to 
the delay of main memory using the write-through scheme.
\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{pictures/diagram_}
	\caption{Diagram of an example memory hierarchy with a stack cache. The core makes a request into the address space which can either
		hit or miss the stack cache window. On a hit the request will be serviced by the stack cache, otherwise it will be served by either
	an L1 cache or go straight to the next level of the hierarchy.}
\end{figure}

\subsection{FPGA resource consumption}
Currently, on-chip memory cannot be inferred from our cache description even though all reads and writes are synchronous. This leads
to a distorted resource consumption report. We estimate that a 128 word cache with a word size of 32 bits and a block size of one word
will consume about 300LEs. We also estimate the resource consumption to scale linearly as the only construct apart from routing that
needs additional LEs is the valid bit register.
